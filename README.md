# Coronamap의 Kubernetes 리소스들을 배포하는 Helm chart
   
### 역할
Kubernetes 리소스들을 간편하게 배포할 수 있을 뿐만 아니라 ArgoCD가 바라보는 repo이다.    
   
      
### 도식화
아래 설명과 이미지는 CICD의 flow를 보여줌으로써 Helm chart의 위치와 역할을 보여준다.
1. 유저가 Coronamap 서비스를 업데이트 한다.
2. Docker image가 생성되는 repo에 commit이 발생한다.
3. Jenkins 서버에서 webhook을 통해 commit을 감지하고 파이프라인을 빌드한다.
4. Jenkins 파이프라인의 output으로 Helm Chart Repo가 업데이트 된다.
5. ArgoCD에서 webhook을 통해 Helm Chart Repo의 commit을 감지한다.
6. sync를 통해 desired status가 k8s cluster에 업데이트된다. 


![Untitled](/uploads/5e153f3d4cceeac39d23c5e1ca4d7a13/Untitled.png)
